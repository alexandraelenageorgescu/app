import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss']
})
export class ToolsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  addNote(): void{
    this.router.navigateByUrl('/addnote');
  }
  title:string= "Add note";

  titleColor:string ="red";

  TitleBackgroundColor:string ="yellow";

  setTitle() { this.title = "test"; }
  
  setbackground() { if(this.TitleBackgroundColor ==="blue")this.TitleBackgroundColor="yellow";
  else if(this.TitleBackgroundColor==="yellow")this.TitleBackgroundColor="blue";
}

  buttonClick(): void {

     alert("button Click");

  }

}
