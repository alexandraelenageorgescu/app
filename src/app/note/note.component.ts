import { Component, OnInit } from '@angular/core';
import { NoteService } from '../services/note.service';
import { Note } from './note';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit
 {
 
  notes: Note[] | undefined; 
  constructor(private noteService: NoteService) { }
  
  ngOnInit(): void { this.noteService.serviceCall();
    this.notes = this.noteService.getNotes();
  }
 


}
